/**
 * 
 */
package kf008772;

/**
 * @author shsmchlr
 * Class of a multi layer perceptron network with training, unseen and validation data sets
 * MLP has hidden layer of sigmnoidally activated neurons and then output layer(s)
 * Such a network can learn using the training set, and be tested on teh unseen set
 * In addition, it can use the validation set to decide when to stop learning
 */
public class MLPwithDataSets extends MultiLayerNetwork {

	// HINT you may need extra variables here
	
	protected DataSet unseenData;			// unseen data set
	protected DataSet validationData;		// validation set : is set to null if that set is not being used
	
	private Double previousSUMofSSE;		// hold the previous sum off SSE in the neural network to added to
	private boolean hasLearned;				// boolean value to check if the network is able to learn or not
	
	/**
	 * Constructor for the MLP
	 * @param numIns			number of inputs	of hidden layer
	 * @param numOuts			number of outputs	of hidden layer
	 * @param data				training data set used
	 * @param nextL				next layer		
	 * @param unseen			unseen data set
	 * @param valid				validation data set
	 *
	 */
	MLPwithDataSets (int numIns, int numOuts, DataSet data, LinearLayerNetwork nextL,
						DataSet unseen, DataSet valid) {
		super(numIns, numOuts, data, nextL);	// create the MLP
												// and store the data sets
		unseenData = unseen;
		validationData = valid;
		
	}

	/** 
	 * initialise network before learning ...
	 * 
	 * @param previousSUMofSSE	a double which holds the value of the previous SUM of SSE that will be compared to
	 * @param hasLearned		a boolean variable which checks if the network can check if it has learned
	 */
	public void doInitialise() {
		super.doInitialise();
		
		previousSUMofSSE = 1000.0;	//set to a maximum of 1000.0 set squared errors
		hasLearned = false; //initialised to default to false to later be changed after learning
	}
	/**
	 * present the data to the set and return a String describing results
	 * Here it returns the performance when the training, unseen (and if available) validation
	 * sets are passed - typically responding with SSE and if appropriate % correct classification
	 */
	public String doPresent() {
		String S;
		presentDataSet(trainData);
		S = "Train: " +  trainData.dataAnalysis();
		presentDataSet(unseenData);
		S = S + " Unseen: " + unseenData.dataAnalysis();
		if (validationData != null) {
			presentDataSet(validationData);
			S = S + " Valid: " + validationData.dataAnalysis();
		}
		return S;
	}

	/**
	 * learn training data, printing SSE at 10 of the epochs, evenly spaced
	 * if a validation set available, learning stops when SSE on validation set rises
	 * this check is done by summing SSE over 10 epochs
	 * @param numEpochs			number of epochs
	 * @param lRate				learning rate
	 * @param momentum			momentum
	 * 
	 * @param s					String value which holds the Epochs
	 * @param epochsSoFar 		Integer value which holds the epochs
	 * @param SSESUM			SSE that holds current SUM of SSE that is initialised to 0
	 * @param ct				Integer value which goes through the number of Epochs
	 * @param trainData			Data set which contains the data which the network learns from
	 * @param validationData	Data set which contains the data that is passed into the network
	 * @param ct2				Integer value which goes through the size of trainData SSE
	 * @param hasLearned		A boolean value which checks if the network has completed learning by checking previous SUM of SSE and SSE Sum
	 * 
	 * @return				String with data about learning eg SSEs at relevant epoch
	 */
	public String doLearn (int numEpochs, double lRate, double momentum) {
		
		//String that will contain the Epochs
		String s = " ";
		
		
		//if the network has already learned, (previousSUMofSSE < SSESum) present an empty string
		if(hasLearned)
		{
			return  s;
		}
		
		//EpochsSoFar of trainData retrieved by the SSELog
		int epochsSoFar = trainData.sizeSSELog();
		
		//set the current SSE to 0 before starting learn loop
		Double SSESum = 0.0; 
		
		//Loop repeats using the number of epochs
			for (int ct=1; ct<=numEpochs; ct++) 
			{	
				//Learn Data using trainingData
				learnDataSet(trainData, lRate, momentum);
				
				//Pass the validation data
				presentDataSet(validationData);
				
				//add SSE of validation data to the SSELog
				validationData.addToSSELog();
				
				
				//Loop adding SSE to the SSE Sum
				for(int ct2=0; ct2 < trainData.getSSE().size(); ct2++)
				{
					//adding to SSESum
					SSESum += validationData.getSSE().get(ct2);
				
				}
				
				
				//Every 10th Epoch
				if (ct%10 == 0)
				{
					//if PreviousSUMofSSE < SSESUM, set hasLearned to true as it has gone through all the epochs
					if(previousSUMofSSE < SSESum)
					{
						//stop learning after the epochs have gone through
						hasLearned = true;
						return s + "Stopped learning after: " + trainData.sizeSSELog() + " epochs. ";
					
					}
					//set the new previusSUMofSSE to the new SSE Sum and then set SSESum to 0 to redo the learning
					else
					{
						previousSUMofSSE = SSESum;
						SSESum =0.0;
					}
					
			}
			
			
				//present data
				if (numEpochs<20 || ct % (numEpochs/10) == 0) 
					s = s + addEpochString(ct+epochsSoFar) + " : " + 
						trainData.dataAnalysis()+"\n";
			}

			//return all epochs and SSE
		return s;											
	}

}
